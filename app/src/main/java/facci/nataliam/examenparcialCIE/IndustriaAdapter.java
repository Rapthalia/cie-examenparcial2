package facci.nataliam.examenparcialCIE;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class IndustriaAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
  private Context context;
  ArrayList<Industria> list = new ArrayList<>();
  private ItemClickListener itemClickListener;

  public IndustriaAdapter(Context ctx, ItemClickListener itemClickListener) {
    this.context = ctx;
    this.itemClickListener = itemClickListener;
  }

  public void setItems(ArrayList<Industria> industrias) {
    list.addAll(industrias);
  }

  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(context).inflate(R.layout.lista_item, parent, false);
    return new IndustriaVH(view);
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
    IndustriaVH vh = (IndustriaVH) holder;
    Industria industria = list.get(position);
    vh.industriaNombreTextView.setText(industria.getIndustriaNombre());
    vh.repesentanteTextView.setText(industria.getRepresentante());

    holder.itemView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        itemClickListener.onItemClick(industria);
      }
    });
  }

  @Override
  public int getItemCount() {
    return list.size();
  }

  public interface ItemClickListener {
    void onItemClick(Industria industria);
  }
}
