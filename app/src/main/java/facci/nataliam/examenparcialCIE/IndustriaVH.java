package facci.nataliam.examenparcialCIE;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class IndustriaVH extends RecyclerView.ViewHolder {
  public TextView industriaNombreTextView, repesentanteTextView;

  public IndustriaVH(@NonNull View itemView) {
    super(itemView);
    industriaNombreTextView = (TextView) itemView.findViewById(R.id.industriaNombreTextView);
    repesentanteTextView = (TextView) itemView.findViewById(R.id.repesentanteTextView);
  }
}
