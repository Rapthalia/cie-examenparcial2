package facci.nataliam.examenparcialCIE;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

public class RegistroActivity extends AppCompatActivity {
    EditText industriaNombreEditText;
    EditText representanteEditText;
    EditText emailEditText;
    EditText areaIndustriaEditText;
    Button saveIndustryButton;
    ImageView btnregresar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        // Button
        saveIndustryButton = (Button) findViewById(R.id.saveIndustryButton);

        //imageView
        btnregresar = (ImageView) findViewById(R.id.btnregresar);

        // texto editable
        industriaNombreEditText = (EditText) findViewById(R.id.industriaNombreEditText);
        representanteEditText = (EditText) findViewById(R.id.representanteEditText);
        emailEditText = (EditText) findViewById(R.id.emailEditText);
        areaIndustriaEditText = (EditText) findViewById(R.id.areaIndustriaEditText);

        saveIndustryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveRegister();
            }
        });

        btnregresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showHome();
            }
        });
    }

    private void saveRegister() {
        DAOIndustria dao = new DAOIndustria();

        // Values
        String industriaNombre = industriaNombreEditText.getText().toString();
        String representante = representanteEditText.getText().toString();
        String email = emailEditText.getText().toString();
        String areaIndustria = areaIndustriaEditText.getText().toString();

        boolean canSave = !industriaNombre.equals("") && !representante.equals("") && !email.equals("") && !areaIndustria.equals("");

        if (canSave) {
            Industria industria = new Industria(industriaNombre, representante, email, areaIndustria);

            // Preparacion del objeto industria
            dao.add(industria).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void unused) {
                    showAlert("¡Agregado!", "Industria agregada correctamente!");
                    clearForm();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    showAlert("Error", "Error al agregar industria! :c");
                }
            });
        } else {
            showAlert("Advertencia", "Debe llenar todos los campos.");
        }
    }

    private void showHome() {
        Intent homeIntent = new Intent(this, InicioActivity.class);
        startActivity(homeIntent);
        finish();
    }

    private void showAlert(String title, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setPositiveButton("Aceptar", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void clearForm() {
        industriaNombreEditText.setText("");
        representanteEditText.setText("");
        emailEditText.setText("");
        areaIndustriaEditText.setText("");
    }


}