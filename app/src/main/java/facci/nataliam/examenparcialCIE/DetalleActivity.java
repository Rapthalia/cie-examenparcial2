package facci.nataliam.examenparcialCIE;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

public class DetalleActivity extends AppCompatActivity {
    String key;
    DAOIndustria dao;
    Industria industria;
    TextView industriaNombreTextView, representanteTextView, emailTextView, areaIndustriaTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);
        // Find Text Views
        industriaNombreTextView = (TextView) findViewById(R.id.industriaNombreTextView);
        representanteTextView = (TextView) findViewById(R.id.representanteTextView);
        emailTextView = (TextView) findViewById(R.id.emailTextView);
        areaIndustriaTextView = (TextView) findViewById(R.id.areaIndustriaTextView);

        key = getIntent().getStringExtra("object_key");
        dao = new DAOIndustria();

        dao.getSingle(key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot data: snapshot.getChildren()) {
                    industria = data.getValue(Industria.class);
                }
                showData();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    private void showData() {
        // Titulo
        setTitle("Industria: " + industria.getIndustriaNombre());

        // Detalles
        industriaNombreTextView.setText(industria.getIndustriaNombre());
        representanteTextView.setText(industria.getRepresentante());
        emailTextView.setText(industria.getEmail());
        areaIndustriaTextView.setText(industria.getAreaIndustria());
    }
}