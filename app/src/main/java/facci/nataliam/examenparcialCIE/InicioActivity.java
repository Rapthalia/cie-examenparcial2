package facci.nataliam.examenparcialCIE;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class InicioActivity extends AppCompatActivity {
    Button registerButton;
    SwipeRefreshLayout swipeRefreshLayout;
    RecyclerView recyclerView;
    IndustriaAdapter adapter;
    DAOIndustria dao;
    boolean isLoading = false;
    String key = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
        setTitle("Cámara de Industrias (CIE)");

        // Button
        registerButton = (Button) findViewById(R.id.registerButton);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRegister();
            }
        });

        // enlista los item
        IndustriaAdapter.ItemClickListener itemClickListener = new IndustriaAdapter.ItemClickListener() {
            @Override
            public void onItemClick(Industria industria) {
                showDetail(industria);
            }
        };

        // Swipper Layout
        swipeRefreshLayout = findViewById(R.id.swiper);
        recyclerView = findViewById(R.id.rv);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        adapter = new IndustriaAdapter(this, itemClickListener);
        recyclerView.setAdapter(adapter);

        // cargar datos
        dao = new DAOIndustria();
        loadData();

        // List listeners
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int totalItem = linearLayoutManager.getItemCount();
                int lastVisible = linearLayoutManager.findLastVisibleItemPosition();

                if (totalItem < lastVisible + 3) {
                    if (!isLoading) {
                        isLoading = true;
                        loadData();
                    }
                }
            }
        });
    }

    private void showRegister() {
        Intent registerIntent = new Intent(this, RegistroActivity.class);
        startActivity(registerIntent);
    }

    private void showDetail(Industria industria) {
        Intent detalleIntent = new Intent(this, DetalleActivity.class);
        detalleIntent.putExtra("object_key", industria.getKey());
        startActivity(detalleIntent);
    }

    private void loadData() {
        swipeRefreshLayout.setRefreshing(true);

        dao.get(key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                ArrayList<Industria> industrias = new ArrayList<>();

                for (DataSnapshot data: snapshot.getChildren()) {
                    Industria indus = data.getValue(Industria.class);
                    indus.setKey(data.getKey());
                    industrias.add(indus);
                    key = data.getKey();
                }

                adapter.setItems(industrias);
                adapter.notifyDataSetChanged();
                isLoading = false;
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }
}