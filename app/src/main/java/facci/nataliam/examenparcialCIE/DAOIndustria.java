package facci.nataliam.examenparcialCIE;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

public class DAOIndustria {
  private DatabaseReference databaseReference;

  public DAOIndustria() {
    FirebaseDatabase db = FirebaseDatabase.getInstance();
    databaseReference = db.getReference(Industria.class.getSimpleName());
  }

  public Task<Void> add(Industria industria) {
    return databaseReference.push().setValue(industria);
  }

  public Query get(String key) {
    if (key == null) {
      return databaseReference.orderByKey().limitToFirst(8);
    }

    return databaseReference.orderByKey().startAfter(key).limitToFirst(8);
  }

  public Query getSingle(String key) {
    return databaseReference.orderByKey().startAt(key).limitToFirst(1);
  }
}
